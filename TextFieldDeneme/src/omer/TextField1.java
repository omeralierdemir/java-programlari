package omer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextField1 extends JPanel implements ActionListener {

	JTextField x1;
	JButton  b1;
	JLabel lb2;
	
	public TextField1() {
		
		super();
		
		x1 = new JTextField(10);
		b1 = new JButton("tamam");
		b1.addActionListener(this);
		lb2 = new JLabel();
		add(x1);
		add(b1);
		add(lb2);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getActionCommand().equals("tamam"))
		{
			lb2.setText(lb2.getText() + x1.getText());
			
			x1.setText("");
			x1.requestFocus();// yazdığımız metnin yazdığımız noktaya sabitliyor 
			
		}
		
		
	}

}
