package Calculator;

public class AnaIslem {
	public double sinBul(String args) {
		double sin = Double.parseDouble(args);
		double asdf = Math.sin(sin);
		return asdf;
	}

	public double cosBul(String args) {
		double cos = Double.parseDouble(args);
		return Math.cos(cos);
	}

	public double tanBul(String args) {
		double tan = Double.parseDouble(args);
		return Math.tan(tan);
	}

	public double cotBul(String args) {
		double sayi = Double.parseDouble(args);
		return 1 / tanBul("" + sayi);
	}

	public double arcsin(String args) {
		double sayi = Double.parseDouble(args);
		return Math.asin(sayi);
	}

	public double arccos(String args) {
		double sayi = Double.parseDouble(args);
		return Math.acos(sayi);
	}

	public double arctan(String args) {
		double sayi = Double.parseDouble(args);
		return Math.atan(sayi);
	}

	public double arccot(String args) {
		double sayi = Double.parseDouble(args);
		return 1 / arctan("" + sayi);
	}

	public double exponansiyelBul(String args) {
		double e = Double.parseDouble(args);
		return Math.exp(e);
	}

	public double tabanaYuvarla(String args) {
		double tabanaYuvarla = Double.parseDouble(args);
		return Math.floor(tabanaYuvarla);
	}

	public double tavanaYuvarla(String args) {
		double tavanaYuvarla = Double.parseDouble(args);
		return Math.ceil(tavanaYuvarla);
	}

	public double logaritmaOnTabaninda(String args) {
		double logaritmaOnTabani = Double.parseDouble(args);
		return Math.log10(logaritmaOnTabani);
	}

	public double logaritmaIkiTabaninda(String args) {
		double logaritmaIkiTabani = Double.parseDouble(args);
		return Math.log(logaritmaIkiTabani);
	}

	public double dereceyiRadyanaCevir(String args) {
		double derece = Double.parseDouble(args);
		return Math.toRadians(derece);
	}

	public double ussunuAl(double tabani, double ussu) {
	//	double taban = Double.parseDouble(tabani);
		//double uzeri = Double.parseDouble(ussu);
		return Math.pow(tabani, ussu);
	}

	public double mutlakDegeriniAl(String args) {
		double sayi = Double.parseDouble(args);
		return Math.abs(sayi);
	}

	public double piSayisi() {
		return Math.PI;
	}

	public double eSayisi() {
		return Math.E;
	}

	public double kareKoku(String args) {
		double kok = Double.parseDouble(args);
		return Math.sqrt(kok);
	}

	public double kupKoku(String args) {
		double kok = Double.parseDouble(args);
		return Math.cbrt(kok);
	}

	public double topla(double args, double args2) {
		
		return args + args2;
	}

	public double topla2(String... args) {

		double[] dizi = new double[args.length];
		int i = 0;
		for (String s : args) {
			dizi[i] = Double.parseDouble(s);
			i++;
		}
		double toplam = 0.0;
		for (int j = 0; j < dizi.length; j++) {
			toplam = toplam + dizi[j];
		}
		return toplam;
	}

	public double cikart(Double args, Double args2) {
		
		return args - args2;
	}

	public double cikart2(String... args) {
		double[] dizi = new double[args.length];
		int i = 0;
		for (String s : args) {
			dizi[i] = Double.parseDouble(s);
			i++;
		}
		double cikartma = dizi[0];
		for (int j = 1; j < dizi.length; j++) {
			cikartma = cikartma - dizi[j];
		}
		return cikartma;
	}

	public double carp(double args, double args2) {
		
		return args * args2;
	}

	public double carp2(String... args) {
		double[] dizi = new double[args.length];
		int i = 0;
		for (String s : args) {
			dizi[i] = Double.parseDouble(s);
			i++;
		}
		double carpim = 1.0;
		for (int j = 0; j < dizi.length; j++) {
			carpim = carpim * dizi[j];
		}
		return carpim;
	}

	public double bol(double args, double args2) {
		
		return args / args2;
	}

	public double bol2(String... args) {
		double[] dizi = new double[args.length];
		int i = 0;
		for (String s : args) {
			dizi[i] = Double.parseDouble(s);
			i++;
		}
		double bolme = dizi[0];
		for (int j = 1; j < dizi.length; j++) {
			bolme = bolme / dizi[j];
		}
		return bolme;
	}

	public double mod(String args, String args2) {
		double sayi1 = Double.parseDouble(args);
		double sayi2 = Double.parseDouble(args2);
		return sayi1 % sayi2;
	}

	public double mod2(String... args) {
		double[] dizi = new double[args.length];
		int i = 0;
		for (String s : args) {
			dizi[i] = Double.parseDouble(s);
			i++;
		}
		double modAl = dizi[0];
		for (int j = 1; j < dizi.length; j++) {
			modAl = modAl % dizi[j];
		}
		return modAl;
	}
}
