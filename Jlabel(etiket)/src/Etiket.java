import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Etiket extends JFrame implements MouseListener{

	JLabel lb1,lb2,lb3;
	JPanel p1 ;
	int sayac=0;
	
	public Etiket(){
		p1 =new JPanel();
		
		lb1 =new JLabel("merhaba");//labeli panele panelide JFrame extends eden classa eklediğimize dikkat et
		p1.add(lb1);//add metodu bir frame veya bir panele eklemekte kulanıyoruz. etiket classı JFrame extends ettiğinden direk ekleye biliyoruz
		
		lb2 = new JLabel("başaracağız İnşallah");
		p1.add(lb2);
		
		lb3 = new JLabel(""+sayac);
		p1.add(lb3);
		addMouseListener(this);
		
		add(p1);
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getButton()==e.BUTTON1)
		{
			sayac++;
			lb3.setText(""+sayac);
			
		}
		else if(e.getButton()==e.BUTTON3)
		{
			sayac--;
			lb3.setText(""+sayac);//bu metot label değerini yeniliyor253
		}
		
		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
