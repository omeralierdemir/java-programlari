package Varyans_01;

public class ActionVaryans {
	
	public double SS1(double[] x, double ort){
		
		
		double snc=0;
		for(int i=0;i<x.length;i++){
			
			snc = Math.pow(x[i]-ort, 2) + snc;		
			
		}
		
		return snc;
		
	}
	
	public double SSE(double[] e){
		
		double snc=0;
		
		for(int i=0;i<3;i++){
			
			snc = e[i]+snc;
			System.out.println("d " + snc);
		}
		
		return snc;
	}
	public double SSB(double[] ort, double[] y){
		
		double snc = 0,z=0,yi=0;
		
		for(int i=0;i<3;i++){
			
			z = (ort[i]*y[i])+z;
			yi = y[i] + yi;
			
		}
		
		z = z/yi;
		
		for(int i=0;i<ort.length;i++){
			
			snc = Math.pow(ort[i]-z,2)*y[i] + snc;		
			
		}
		
		return snc;
		
	}

	public double degF(double mSSB, double mSSE, double[] y ){
		
		double snc,mssb,msse,yi;
		
		yi = y[0] + y[1] + y[2];
		mssb = mSSB/(y.length-1);
		msse = mSSE/(yi-y.length);
		
		snc = mssb/msse;
		
		return snc;	
		
	}
	
	
	
}
