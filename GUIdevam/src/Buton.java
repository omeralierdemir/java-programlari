import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Buton extends JPanel implements ActionListener{
	
	JButton b1,b2;
	int a=0;
	JLabel l1;
	public Buton(){
		
		super();
		b1 =new JButton("artt�r");
		b2 = new JButton("azalt");
		add(b1);
		add(b2);
		b1.addActionListener(this);
		b2.addActionListener(this);
		l1 =new JLabel("0");
		add(l1);
		
	}

	public static void main(String[] args) {
		
		JFrame cer = new JFrame("�er�eve");//reis genel olarak Jframe ile paneli main metot i�inde olu�turp eklemelerini yapyoruz
		Buton p1 = new Buton();
		cer.add(p1);
		
		cer.setSize(640, 480);
		cer.setLocationRelativeTo(null);
		cer.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
			
		if(e.getActionCommand().equals("artt�r"))
		{
		
			a =Integer.parseInt(l1.getText());
			a++;
			l1.setText("" + a);
			
		}
		else if(e.getActionCommand().equals("azalt"))//GetActionCommand (), eylem komutunu temsil eden bir String verir yani burda buton i�inde yazan� d�nd�rd�k
		{
			
			a =Integer.parseInt(l1.getText());// burda label etiketinin i�inde stringin say�sal terimi �nteger yapt�k sonra a ya e�itledik gettext ile is etiketin i�eri�ini ald�k
			a--;//�steki komutsuzda �al���yor :)))))
			l1.setText("" + a);
			
		}
		
	}

}
