package omer.ali.erdemir;

public class Mudur extends Calisan{
	
	private int maas;
	
	public int getMaas() {
		return maas;
	}



	public void setMaas(int maas) {
		this.maas = maas;
	}



	public int getKacAyCalisti() {
		return kacAyCalisti;
	}



	public void setKacAyCalisti(int kacAyCalisti) {
		this.kacAyCalisti = kacAyCalisti;
	}



	public static int getZamkatsayisi() {
		return zamKatsayisi;
	}



	private int kacAyCalisti;
	
	private static final int zamKatsayisi=100; 
	
	public Mudur(String name, int yas, int maas){
		super(name,yas);
		this.maas=maas;
	}
	
	
	
	public int zam(){
		return (maas * kacAyCalisti)/zamKatsayisi;
	}
	
	
}
