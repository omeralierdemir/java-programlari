package omer.ali.erdemir;

public abstract class Calisan {
	
	private String name;
	private int yas;
	
	public Calisan(){}
	
	public Calisan(String name, int yas){
		this.name=name;
		this.yas=yas;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYas() {
		return yas;
	}

	public void setYas(int yas) {
		this.yas = yas;
	}
	
}
