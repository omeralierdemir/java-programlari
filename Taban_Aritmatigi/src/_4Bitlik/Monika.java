package _4Bitlik;

import java.awt.Desktop.Action;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.naming.ldap.Rdn;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

public class Monika implements ItemListener {

	ActionTransform a3 = new ActionTransform();
	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JLabel lbl1,lbl2,lbl3,lbl4,lbl5,lbl6,lbl7,lbl8;
	private JButton btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8;
	private JRadioButton rd1,rd2,rd3,rd4,rd5,rd6,rd7,rd8;
	private ButtonGroup bg = new ButtonGroup();
	private ImageIcon ikon = new ImageIcon("C:/Users/casper/Desktop/javam/Taban_Aritmatigi/src/Ikonlar/imagess.jpg");
	private boolean[] nonSelected = new boolean[] { false, false, false, false, false, false, false, false };
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Monika window = new Monika();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Monika() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(720, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		 rd1 = new JRadioButton("2'lik tabandan 10'luk tabana \u00E7evirme");
		
		rd1.setBounds(6, 22, 250, 23);
		frame.getContentPane().add(rd1);
		rd1.addItemListener(this);
		
		rd2 = new JRadioButton("10'luk tabandan 2'lik tabana \u00E7evirme");
		rd2.setBounds(6, 48, 250, 23);
		frame.getContentPane().add(rd2);
		rd2.addItemListener(this);
		
		rd3 = new JRadioButton("2'lik tabandan 8'lik tabana \u00E7evirme");
		rd3.setBounds(6, 74, 250, 23);
		frame.getContentPane().add(rd3);
		rd3.addItemListener(this);
		
		rd4 = new JRadioButton("8'lik tabandan 2'lik tabana \u00E7evirme");
		rd4.setBounds(6, 100, 250, 23);
		frame.getContentPane().add(rd4);
		rd4.addItemListener(this);
		
		rd5 = new JRadioButton("10'luk tabandan 16'l\u0131k tabana \u00E7evirme");
		rd5.setBounds(6, 126, 250, 23);
		frame.getContentPane().add(rd5);
		rd5.addItemListener(this);
		
		rd6 = new JRadioButton("16'l\u0131k tabandan 10'luk tabana \u00E7evirme");
		rd6.setBounds(6, 156, 250, 23);
		frame.getContentPane().add(rd6);
		rd6.addItemListener(this);
		
		rd7 = new JRadioButton("8'lik tabandan 10'luk tabana \u00E7evirme");
		rd7.setBounds(6, 182, 250, 23);
		frame.getContentPane().add(rd7);
		rd7.addItemListener(this);
		
		rd8 = new JRadioButton("10'luk tabandan 8'lik tabana \u00E7evirme");
		rd8.setBounds(6, 212, 250, 23);
		frame.getContentPane().add(rd8);
		rd8.addItemListener(this);
	
		
	    lbl1 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl1.setBounds(262, 26, 214, 14);
		frame.getContentPane().add(lbl1);
		lbl1.setVisible(false);
		
	    lbl2 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl2.setBounds(262, 52, 214, 14);
		frame.getContentPane().add(lbl2);
		lbl2.setVisible(false);
		
		lbl3 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl3.setBounds(262, 78, 214, 14);
		frame.getContentPane().add(lbl3);
		lbl3.setVisible(false);
		
	    lbl4 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl4.setBounds(262, 104, 214, 14);
		frame.getContentPane().add(lbl4);
		lbl4.setVisible(false);
		
	    lbl5 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl5.setBounds(262, 130, 214, 14);
		frame.getContentPane().add(lbl5);
		lbl5.setVisible(false);
		
		lbl6 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl6.setBounds(262, 160, 214, 14);
		frame.getContentPane().add(lbl6);
		lbl6.setVisible(false);
		
		lbl7 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl7.setBounds(262, 186, 214, 14);
		frame.getContentPane().add(lbl7);
	    lbl7.setVisible(false);
		
		lbl8 = new JLabel("d\u00F6n\u00FC\u015Ft\u00FCrmek istedi\u011Finiz de\u011Feri giriniz");
		lbl8.setBounds(262, 216, 214, 14);
		frame.getContentPane().add(lbl8);
		lbl8.setVisible(false);
		
		textField = new JTextField();
		textField.setBounds(486, 23, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.setVisible(false);
		
		textField_1 = new JTextField();
		textField_1.setBounds(486, 49, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		textField_1.setVisible(false);
		
		textField_2 = new JTextField();
		textField_2.setBounds(486, 75, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
	    textField_2.setVisible(false);
		
		textField_3 = new JTextField();
		textField_3.setBounds(486, 101, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		textField_3.setVisible(false);
		
		textField_4 = new JTextField();
		textField_4.setBounds(486, 127, 86, 20);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		textField_4.setVisible(false);
		
		textField_5 = new JTextField();
		textField_5.setBounds(486, 157, 86, 20);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		textField_5.setVisible(false);
		
		textField_6 = new JTextField();
		textField_6.setBounds(486, 183, 86, 20);
		frame.getContentPane().add(textField_6);
		textField_6.setColumns(10);
		textField_6.setVisible(false);
		
		textField_7 = new JTextField();
		textField_7.setBounds(486, 213, 86, 20);
		frame.getContentPane().add(textField_7);
		textField_7.setColumns(10);
		textField_7.setVisible(false);
		
		btn1 = new JButton("tamam");
		btn1.setBounds(591, 22, 89, 23);
		frame.getContentPane().add(btn1);
		btn1.setVisible(false);
		
		btn2 = new JButton("tamam");
		btn2.setBounds(591, 48, 89, 23);
		frame.getContentPane().add(btn2);
		btn2.setVisible(false);
		
		
		btn3 = new JButton("tamam");
		btn3.setBounds(591, 74, 89, 23);
		frame.getContentPane().add(btn3);
		btn3.setVisible(false);
		
		btn4 = new JButton("tamam");
		btn4.setBounds(591, 100, 89, 23);
		frame.getContentPane().add(btn4);
		btn4.setVisible(false);
		
		btn5 = new JButton("tamam");
		btn5.setBounds(591, 126, 89, 23);
		frame.getContentPane().add(btn5);
		btn5.setVisible(false);
		
		btn6 = new JButton("tamam");
		btn6.setBounds(591, 156, 89, 23);
		frame.getContentPane().add(btn6);
		btn6.setVisible(false);
		
		btn7 = new JButton("tamam");
		btn7.setBounds(591, 182, 89, 23);
		frame.getContentPane().add(btn7);
		btn7.setVisible(false);
		
		btn8 = new JButton("tamam");
		btn8.setBounds(591, 212, 89, 23);
		frame.getContentPane().add(btn8);
		btn8.setVisible(false);
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String number;
				double snc,kpr,sayac=0;
				for(int i=0;i<textField.getText().length();i++)
				{
					if(textField.getText().charAt(i)!='0' && textField.getText().charAt(i)!='1')
					{
						sayac++;
					}
				}
			if(e.getActionCommand().equals("tamam"))
			{
				System.out.println(sayac);
				if(sayac!=0)
				{
				
					JOptionPane.showMessageDialog(lbl3, "Yanl�� de�er giridiniz... L�tfen kontrol ediniz","LOLLLLLLLL",0,ikon);
					
				}
				else
				{
						
			    snc = Double.parseDouble(textField.getText());
			    kpr = a3.Transform2_10(snc);
			    number = Double.toString(kpr);
			    lbl1.setText(number);
				}
			
			}
				
				
			}
		});
		
	
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				

				String number;
				double snc;
				
				if(e.getActionCommand().equals("tamam"))
				{
					snc = Double.parseDouble(textField_1.getText());
					number = a3.Transform10_2(snc);
					
					lbl2.setText(number);
					
				}
				
			}
		});
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
		
				String number;
				double snc,kpr,sayac=0;
				
		
		
					for(int i=0;i<textField_2.getText().length();i++)
					{
						if(textField_2.getText().charAt(i)!='0' && textField_2.getText().charAt(i)!='1')
						{
							sayac++;
						}
					}
				if(e.getActionCommand().equals("tamam"))
				{
					System.out.println(sayac);
					if(sayac!=0)
					{
					
						JOptionPane.showMessageDialog(lbl3, "Yanl�� de�er giridiniz... L�tfen kontrol ediniz","LOLLLLLLLL",0,ikon);
						
					}
					else
					{
							
						 snc = Double.parseDouble(textField_2.getText());
						 snc = a3.Transform2_8(snc);
						 lbl3.setText(" " + snc);
					}
				
				}
				
				
				
		}
	});
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double snc1;
				
		if(e.getActionCommand().equals("tamam"))
		{
			for(int i=0;i<textField_3.getText().length();i++)
			{
				snc1 = Double.parseDouble(" " + textField_3.getText().charAt(i));
				if(snc1<=7 && snc1>=0 )
				{
					String number;
					double snc;
					
				
				
						snc = Double.parseDouble(textField_3.getText());
						number = a3.Transform8_2(snc);
						lbl4.setText("  " + number);
				}
					else
					{
						JOptionPane.showMessageDialog(lbl4, "Yanl�� de�er giridiniz... L�tfen kontrol ediniz","LOLLLLLLLL",0,ikon);
						break;
					}
				
			}
			
		}
				
				
			}
		});
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				 String number;
				 double snc;
				
				 if(e.getActionCommand().endsWith("tamam"))
				 {
					 snc = Double.parseDouble(textField_4.getText());
					 number = a3.Transform10_16(snc);
					 lbl5.setText(" " + number);
					 
				 }
			}
		});
		btn6.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
			 double snc;
			
			 if(e.getActionCommand().equals("tamam"))
			 {
				 snc = a3.Transform16_10(textField_5.getText());
				 lbl6.setText(" " + snc);
				 
			 }
			}
		});
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				double snc1;
				
		if(e.getActionCommand().equals("tamam"))
		{
			for(int i=0;i<textField_6.getText().length();i++)
			{
				snc1 = Double.parseDouble(" " + textField_6.getText().charAt(i));
				if(snc1<=7 && snc1>=0 )
				{
				
					double snc;
					
				
					 snc = Double.parseDouble(textField_6.getText());
					 snc = a3.Transform8_10(snc);
					 lbl7.setText(" " + snc);
				}
					else
					{
						JOptionPane.showMessageDialog(lbl7, "Yanl�� de�er giridiniz... L�tfen kontrol ediniz","LOLLLLLLLL",0,ikon);
						break;
					}
				
			}
			
		}
			}
		});
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				

				 double snc;
				
				 if(e.getActionCommand().equals("tamam"))
				 {
					 snc = Double.parseDouble(textField_7.getText());
					 snc = a3.Transform10_8(snc);
					 lbl8.setText(" " + snc);
				 }
				
			}
		});
		
	
	}


	@Override
	public void itemStateChanged(ItemEvent e) {
		int d1 = e.getStateChange();

		if (rd1.isSelected()) {
			lbl1.setVisible(true);
			textField.setVisible(true);
			btn1.setVisible(true);
			nonSelected[0] = true;
		}
		if (!rd1.isSelected() && nonSelected[0]) {
			lbl1.setVisible(false);
			textField.setVisible(false);
			btn1.setVisible(false);
			nonSelected[0] = false;
		}

		if (rd2.isSelected()) {

			lbl2.setVisible(true);
			textField_1.setVisible(true);
			btn2.setVisible(true);
			nonSelected[1] = true;
		}

		if (!rd2.isSelected() && nonSelected[1]) {
			lbl2.setVisible(false);
			textField_1.setVisible(false);
			btn2.setVisible(false);
			nonSelected[1] = false;
		}

		if (rd3.isSelected()) {
			lbl3.setVisible(true);
			textField_2.setVisible(true);
			btn3.setVisible(true);
			nonSelected[2] = true;
		}

		if (!rd3.isSelected() && nonSelected[2]) {
			lbl3.setVisible(false);
			textField_2.setVisible(false);
			btn3.setVisible(false);
			nonSelected[2] = false;
		}

		if (rd4.isSelected()) {
			lbl4.setVisible(true);
			textField_3.setVisible(true);
			btn4.setVisible(true);
			nonSelected[3] = true;
		}

		if (!rd4.isSelected() && nonSelected[3]) {
			lbl4.setVisible(false);
			textField_3.setVisible(false);
			btn4.setVisible(false);
			nonSelected[3] = false;
		}

		if (rd5.isSelected()) {
			lbl5.setVisible(true);
			textField_4.setVisible(true);
			btn5.setVisible(true);
			nonSelected[4] = true;
		}

		if (!rd5.isSelected() && nonSelected[4]) {
			lbl5.setVisible(false);
			textField_4.setVisible(false);
			btn5.setVisible(false);
			nonSelected[4] = false;
		}

		if (rd6.isSelected()) {
			lbl6.setVisible(true);
			textField_5.setVisible(true);
			btn6.setVisible(true);
			nonSelected[5] = true;
		}

		if (!rd6.isSelected() && nonSelected[5]) {
			lbl6.setVisible(false);
			textField_5.setVisible(false);
			btn6.setVisible(false);
			nonSelected[5] = false;
		}

		if (rd7.isSelected()) {
			lbl7.setVisible(true);
			textField_6.setVisible(true);
			btn7.setVisible(true);
			nonSelected[6] = true;
		}

		if (!rd7.isSelected() && nonSelected[6]) {
			lbl7.setVisible(false);
			textField_6.setVisible(false);
			btn7.setVisible(false);
			nonSelected[6] = false;
		}

		if (rd8.isSelected()) {
			lbl8.setVisible(true);
			textField_7.setVisible(true);
			btn8.setVisible(true);
			nonSelected[7] = true;
		}
		if (!rd8.isSelected() && nonSelected[7]) {
			lbl8.setVisible(false);
			textField_7.setVisible(false);
			btn8.setVisible(false);
			nonSelected[7] = false;
		}

	}

}	