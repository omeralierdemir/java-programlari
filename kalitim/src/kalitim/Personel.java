package kalitim;

class Personel {
	
	 private String isim,soyisim;
	 private int yas;
	 
	 public Personel(String isim,String soyisim,int yas){
		 
		 this.isim=isim;
		 this.soyisim=soyisim;
		 setYas(yas);
		 
	 }
	 
	
	public String getIsim() {
		return isim;
	}
	public void setIsim(String isim) {
		this.isim = isim;
	}
	public String getSoyisim() {
		return soyisim;
	}
	public void setSoyisim(String soyisim) {
		this.soyisim = soyisim;
	}
	public int getYas() {
		return yas;
	}
	public void setYas(int yas) {
		if(yas<0)
		{
			yas=0;
		}
		else
		{
			this.yas = yas;
		}
		
	}
	
	public void bilgiler(){
		
		System.out.println("isim:" + getIsim() + "soyisim:" + getSoyisim() + "yas=" + getYas());
		
	}
	
}

	 
	