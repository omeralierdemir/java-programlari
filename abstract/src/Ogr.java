
public abstract class Ogr {

	private String isim,soyisim;
	private int yas;
	public Ogr(String isim,String soyisim,int yas) {
		
		this.isim=isim;
		this.soyisim=soyisim;
		setYas(yas);
	
	
	
	
	}
	public abstract void Kullan();
	
	
	
	
	public String getIsim() {
		return isim;
	}
	public void setIsim(String isim) {
		this.isim = isim;
	}
	public String getSoyisim() {
		return soyisim;
	}
	public void setSoyisim(String soyisim) {
		this.soyisim = soyisim;
	}
	public int getYas() {
		return yas;
	}
	public void setYas(int yas) {
		if(yas<0)
		{
			yas=0;			
		}
		else
		{
			this.yas = yas;
		}
		
	}
		
	public String getBilgi(){
		
		return "�smi: " + getIsim() + " Soyisim: " + getSoyisim() + " Yasi= " + getYas();
				 
	}
	
	
	
}
